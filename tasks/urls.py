from django.urls import path
from .views import create_task, my_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", my_task, name="show_my_tasks"),
]
